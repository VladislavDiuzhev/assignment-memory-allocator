//
// Created by vladislav on 21.11.22.
//
#include "tests/tests.h"
#include <stdio.h>

int main(void) {
    if (test_simple_allocation(true)) {
        printf("Test 1 completed successfully!!\n\n");
    } else {
        printf("Test 1 failed!!\n\n");
    }

    if (test_free_one_of_allocated(true)) {
        printf("Test 2 completed successfully!!\n\n");
    } else {
        printf("Test 2 failed!!\n\n");
    }

    if (test_free_two_of_allocated(true)) {
        printf("Test 3 completed successfully!!\n\n");
    } else {
        printf("Test 3 failed!\n\n");
    }

    if (test_new_heap_region_simple(true)) {
        printf("Test 4 completed successfully!\n\n");
    } else {
        printf("Test 4 failed!\n\n");
    }

    if (test_new_heap_region_failed(true)) {
        printf("Test 5 completed successfully!\n\n");
    } else {
        printf("Test 5 failed!\n\n");
    }
    return 0;
}
