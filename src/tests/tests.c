//
// Created by vladislav on 21.11.22.
//
#include <unistd.h>
#include "tests.h"
#include "../utils/mem.h"
#include "../utils/mem_internals.h"

#define SMALL_REGION_SIZE 1
#define MEDIUM_REGION_SIZE 400
#define LARGE_REGION_SIZE 1000
#define X_LARGE_REGION 10000

int getpagesize();

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

bool test_simple_allocation(bool print_info) {
    if (print_info) {
        printf("Allocate 100, 400, 1. Expecting blocks capacity: 100, 400, 24.\n");
    }
    void *b1 = _malloc(MEDIUM_REGION_SIZE);
    void *b2 = _malloc(2 * MEDIUM_REGION_SIZE);
    void *b3 = _malloc(SMALL_REGION_SIZE);
    if (print_info) {
        debug_heap(stdout, HEAP_START);
    } else {
        debug_heap(stderr, HEAP_START);
    }
    return block_get_header(b1)->capacity.bytes == MEDIUM_REGION_SIZE && block_get_header(b2)->capacity.bytes == 2*MEDIUM_REGION_SIZE &&
           block_get_header(b3)->capacity.bytes == 24;
}

bool test_free_one_of_allocated(bool print_info) {
    void *b1 = _malloc(LARGE_REGION_SIZE);
    if (print_info) {
        printf("Allocate 1000.\n");
        debug_heap(stdout, HEAP_START);
    } else {
        debug_heap(stderr, HEAP_START);
    }


    _free(b1);
    if (print_info) {
        printf("Free 1000.\n");
        debug_heap(stdout, HEAP_START);
    } else {
        debug_heap(stderr, HEAP_START);
    }
    return block_get_header(b1)->is_free;
}

bool test_free_two_of_allocated(bool print_info) {
    void *b1 = _malloc(LARGE_REGION_SIZE);
    void *b2 = _malloc(2*LARGE_REGION_SIZE);
    if (print_info) {
        printf("Allocate 1000, 2000.\n");
        debug_heap(stdout, HEAP_START);
    } else {
        debug_heap(stderr, HEAP_START);
    }
    _free(b1);
    _free(b2);
    if (print_info) {
        printf("Free 1000, 2000.\n");
        debug_heap(stdout, HEAP_START);
    } else {
        debug_heap(stderr, HEAP_START);
    }
    return block_get_header(b1)->is_free && block_get_header(b2)->is_free;
}

bool test_new_heap_region_simple(bool print_info) {
    void *b1 = _malloc(X_LARGE_REGION);
    if (print_info) {
        printf("Allocate 10000. Expecting heap size increased (>REGION_MIN_SIZE = 2 * 4096).\n");
        debug_heap(stdout, HEAP_START);
    } else {
        debug_heap(stderr, HEAP_START);
    }
    return block_get_header(b1)->capacity.bytes == X_LARGE_REGION;
}

bool test_new_heap_region_failed(bool print_info) {
    if (print_info) {
        printf("Allocating new region after heap\n");
        debug_heap(stdout, HEAP_START);
    } else {
        debug_heap(stderr, HEAP_START);
    }
    struct block_header *end_heap = NULL;
    for (struct block_header *i = HEAP_START; i; i = i->next) {
        end_heap = i;
    }
    end_heap += (size_t) size_from_capacity(end_heap->capacity).bytes;
    mmap((void *) end_heap, MEDIUM_REGION_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE, 0, 0);
    void *b1 = _malloc(getpagesize());
    if (print_info) {
        printf("Out of memory, finding new region\n");
        debug_heap(stdout, HEAP_START);
    } else {
        debug_heap(stderr, HEAP_START);
    }
    return (size_t) block_get_header(b1)->capacity.bytes == (size_t) getpagesize();
}
