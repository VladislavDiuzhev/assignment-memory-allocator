//
// Created by vladislav on 21.11.22.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H

#include <stdbool.h>

typedef bool test_func(bool print_info);

test_func test_simple_allocation;

test_func test_free_one_of_allocated;

test_func test_free_two_of_allocated;

test_func test_new_heap_region_simple;

test_func test_new_heap_region_failed;

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
