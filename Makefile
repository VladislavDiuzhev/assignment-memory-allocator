CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
BUILDDIR=build
BUILDDIR_O= $(BUILDDIR)/o_files
SRCDIR=src
SRCDIR_UTILS= $(SRCDIR)/utils
SRCDIR_TESTS= $(SRCDIR)/tests
CC=gcc

all: $(BUILDDIR_O)/mem.o $(BUILDDIR_O)/util.o $(BUILDDIR_O)/mem_debug.o $(BUILDDIR_O)/tests.o $(BUILDDIR_O)/main.o
	$(CC) -o $(BUILDDIR)/main $^

build:
	mkdir -p $(BUILDDIR)
	mkdir -p $(BUILDDIR_O)

$(BUILDDIR_O)/mem.o: $(SRCDIR_UTILS)/mem.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR_O)/mem_debug.o: $(SRCDIR_UTILS)/mem_debug.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR_O)/util.o: $(SRCDIR_UTILS)/util.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR_O)/tests.o : $(SRCDIR_TESTS)/tests.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR_O)/main.o : $(SRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)

